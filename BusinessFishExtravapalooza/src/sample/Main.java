package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.shape.*;
import javafx.scene.transform.Rotate;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {

    //public static ArrayList<String> coordinates = new ArrayList<String>();
    //public static int rectCount = 0;
    public static Circle[] globalCircle = new Circle[2];
    public static boolean drawingLine;

    public static String lineType;

    public Image image = new Image("sample/redX.png");  //pass in the image path

    private Group root = new Group();
    Rectangle rectangle;

    private void init(Stage primaryStage) {

        //Instantiation of Menubar
        final Menu menu1 = new Menu("File");
        MenuItem itemOpen = new MenuItem("Open");
        MenuItem itemSave = new MenuItem("Save");
        menu1.getItems().add(itemOpen);
        menu1.getItems().add(itemSave);

        final Menu menu2 = new Menu("Options");
        MenuItem itemAddCB = new MenuItem("Add ClassBox");
        MenuItem itemDeleteObj = new MenuItem("Delete Object");
        MenuItem itemDeleteAll = new MenuItem("Delete All Objects");
        menu2.getItems().add(itemAddCB);
        menu2.getItems().add(itemDeleteObj);
        menu2.getItems().add(itemDeleteAll);


        Menu addNewLine = new Menu("Add Line");

        MenuItem Association = new MenuItem("Association");
        MenuItem dAssociation = new MenuItem("Directed Association");
        MenuItem Inheritance = new MenuItem("Inheritance");
        MenuItem Dependency = new MenuItem("Dependency");
        MenuItem Aggregation = new MenuItem("Aggregation");
        MenuItem Composition = new MenuItem("Composition");

        addNewLine.getItems().addAll(Association, dAssociation, Inheritance, Dependency, Aggregation, Composition);

        menu2.getItems().add(addNewLine);

        final Menu menu3 = new Menu("Help");
        MenuItem helpItem = new MenuItem("Show Help...");
        helpItem.setOnAction(event -> {
            ShowPopup("Help", "Looking for help?", "Consult the documentation for our application to answer any questions!");
        });
        menu3.getItems().addAll(helpItem);

        final Menu menu4 = new Menu("About");
        MenuItem aboutItem = new MenuItem("About...");
        aboutItem.setOnAction(event -> {
            ShowPopup("About", "Welcome to our UML Editor!", "Copyright Business Fish LLC.\nAny attempt to copy or replicate our work will result in a flogging of epic proportions.");
        });
        menu4.getItems().addAll(aboutItem);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(menu1, menu2, menu3, menu4);

        Scene scene = new Scene(root, 750, 600);
        root.setAutoSizeChildren(true);

        //Mac Menu bar
        if (System.getProperty("os.name").contains("Mac")) {
            menuBar.setUseSystemMenuBar(true);
            //com.apple.eawt.Application.getApplication().setDockIconImage(new ImageIcon().getImage());
        } else {
            menuBar.setLayoutX(root.getLayoutX());
            menuBar.setLayoutY(root.getLayoutY());
            menuBar.setMaxWidth(primaryStage.getMaxWidth());
            menuBar.setMinWidth(primaryStage.getMinWidth());
            menuBar.setPrefWidth(scene.getWidth());
        }


        primaryStage.setScene(scene);
        root.getChildren().addAll(menuBar);


        scene.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
                menuBar.setPrefWidth(newSceneWidth.doubleValue());
            }
        });


        //Save function records classbox and line coordinates/attributes
        itemSave.setOnAction(new EventHandler<ActionEvent>() {
            //CODE AUGMENTED FROM:
            //http://java-buddy.blogspot.com/2012/05/save-file-with-javafx-filechooser.html
            @Override
            public void handle(ActionEvent event) {
                ObservableList<Node> list = root.getChildren();
                String allNodes = "";

                for (int i = 0; i < list.size(); ++i) {
                    if (list.get(i) instanceof Rectangle) {
                        Rectangle rect1 = (Rectangle) list.get(i);
                        Double rectX = rect1.getLayoutX();
                        Double rectY = rect1.getLayoutY();

                        String currentID = rect1.getId();

                        String rectInfo = rect1.getAccessibleText();
                        if (rectInfo == null)
                        {
                            rectInfo = "null";
                        }
                        String rectangleOperationAttributes = rect1.getAccessibleRoleDescription();

                        if (currentID != null) {
                            rect1.setId(rectX.toString() + ", " + rectY.toString() + ", " + rectInfo + ", " + rectangleOperationAttributes);
                        }
                    } else if (list.get(i) instanceof Line) {
                        try {
                            Line line = (Line) list.get(i);
                            String[] parts = line.getId().split(", ");
                            String lineType = parts[0];

                            line.setId(lineType + ", " + line.startXProperty().getValue() + ", " + line.startYProperty().getValue() + ", " + line.endXProperty().getValue() + ", " + line.endYProperty().getValue());
                        } catch (Exception e) {
                            Line line = (Line) list.get(i);

                            line.setId(line.getAccessibleText() + ", " + line.startXProperty().getValue() + ", " + line.startYProperty().getValue() + ", " + line.endXProperty().getValue() + ", " + line.endYProperty().getValue());
                        }
                    }
                }

                //Displays elements to ensure correct position recording

                for (int i = 0; i < list.size(); ++i) {
                    if (list.get(i) instanceof Rectangle) {
                        if (list.get(i).getId() != null) {
                            System.out.println("Classbox, " + list.get(i).getId());
                            String id = "Classbox, " + list.get(i).getId();
                            /*String[] parts = list.get(i).getId().split(", ");
                            String layX = parts[0];
                            String layY = parts[1];
                            Rectangle rectangle = new Rectangle();
                            rectangle.setHeight(50);
                            rectangle.setWidth(50);
                            rectangle.setLayoutX(Double.parseDouble(layX));
                            rectangle.setLayoutY(Double.parseDouble(layY));
                            root.getChildren().add(rectangle);*/

                            allNodes += id + "\n";
                        }
                    }
                }

                for (int i = 0; i < list.size(); ++i) {
                    if (list.get(i) instanceof Line) {
                        try {
                            System.out.println("Line, " + list.get(i).getId());
                            String id = "Line, " + list.get(i).getId();
                            /*String[] parts = list.get(i).getId().split(", ");
                            String startLayoutX = parts[1];
                            String startLayoutY = parts[2];
                            String endLayoutX = parts[3];
                            String endLayoutY = parts[4];
                            Line line = new Line();
                            DoubleProperty startX = new SimpleDoubleProperty(Double.parseDouble(startLayoutX));
                            DoubleProperty startY = new SimpleDoubleProperty(Double.parseDouble(startLayoutY));
                            DoubleProperty endX = new SimpleDoubleProperty(Double.parseDouble(endLayoutX));
                            DoubleProperty endY = new SimpleDoubleProperty(Double.parseDouble(endLayoutY));

                            line.setStrokeWidth(5);
                            line.startXProperty().bind(startX);
                            line.startYProperty().bind(startY);
                            line.endXProperty().bind(endX);
                            line.endYProperty().bind(endY);

                            root.getChildren().add(line);*/

                            allNodes += id + "\n";
                        } catch (Exception e) {
                            //System.out.println("this is the created test line");
                        }
                    }
                }

                FileChooser fileChooser = new FileChooser();

                //Set extension filter
                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
                fileChooser.getExtensionFilters().add(extFilter);

                //Show save file dialog
                File file = fileChooser.showSaveDialog(primaryStage);

                if (file != null) {
                    SaveFile(allNodes, file);
                }
            }
        });


        //Open function parses a given text file and recreates the scene, in lieu of XML
        itemOpen.setOnAction(new EventHandler<ActionEvent>() {
            //CODE AUGMENTED FROM
            //https://www.caveofprogramming.com/java/java-file-reading-and-writing-files-in-java.html#readtext
            @Override
            public void handle(ActionEvent event) {
                FileChooser fileChooser = new FileChooser();
                File selectedFile = fileChooser.showOpenDialog(null);
                String line = null;

                if (selectedFile != null) {
                    try {
                        // FileReader reads text files in the default encoding.
                        FileReader fileReader = new FileReader(selectedFile);

                        // Always wrap FileReader in BufferedReader.
                        BufferedReader bufferedReader = new BufferedReader(fileReader);

                        while ((line = bufferedReader.readLine()) != null) {
                            System.out.println(line);

                            String[] parts = line.split(", ");
                            String objectType = parts[0];

                            int u = 0;

                            if (objectType.trim().equals("Classbox")) {
                                String layX = parts[1];
                                String layY = parts[2];

                                String rectTitle = "";

                                try
                                {
                                    rectTitle = parts[3].trim();
                                }
                                catch (Exception e)
                                {

                                }

                                int size = parts.length;

                                Label[] attributes = new Label[10];
                                Label[] operations = new Label[10];

                                for (int i = 0; i < 10; ++i) {
                                    Label label = new Label();
                                    attributes[i] = label;

                                    Label label1 = new Label();
                                    operations[i] = label1;
                                }

                                int attributeCounter = 0;
                                int operationCounter = 0;

                                for (int i = 0; i < size; i++)
                                {
                                    String[] additionalInfo;
                                    String[] temp;
                                    String bitOfInfo = "";

                                    if (parts[i].trim().startsWith("ATTR"))
                                    {
                                        additionalInfo = line.split("ATTR");
                                        bitOfInfo = additionalInfo[1].trim();
                                        temp = bitOfInfo.split(", ");
                                        attributes[attributeCounter].setText(temp[0]);
                                        attributeCounter++;
                                    }
                                    else if (parts[i].trim().startsWith("OPER"))
                                    {
                                        additionalInfo = line.split("OPER");
                                        bitOfInfo = additionalInfo[1].trim();
                                        temp = bitOfInfo.split(",");
                                        operations[operationCounter].setText(temp[0]);
                                        operationCounter++;
                                    }
                                }


                                classboxFactory factory = new classboxFactory();
                                rectangle = factory.createClassbox(root, scene);

                                factory.onOpen(Double.parseDouble(layX), Double.parseDouble(layY), rectTitle, root, attributes, operations);


                                rectangle.setLayoutX(Double.parseDouble(layX));
                                rectangle.setLayoutY(Double.parseDouble(layY));

                            } else if (objectType.trim().equals("Line")) {
                                lineType = parts[1].trim();
                                lineVariants variants1 = new lineVariants();
                                Node[] myShapes = variants1.lineHelper(root);

                                Line line1 = (Line) myShapes[0];
                                Polygon polygon = (Polygon) myShapes[1];
                                Polyline polyline = (Polyline) myShapes[2];

                                String typeLine = parts[1].trim();
                                line1.setAccessibleText(typeLine);


                                ObservableList<Node> list = root.getChildren();

                                for (int i = 0; i < list.size(); ++i) {
                                    if (list.get(i) instanceof Circle) {
                                        if (list.get(i).layoutXProperty().getValue() == Double.parseDouble(parts[2])) {
                                            line1.startXProperty().bind(list.get(i).layoutXProperty());
                                            line1.startYProperty().bind(list.get(i).layoutYProperty());
                                        } else if (list.get(i).layoutYProperty().getValue() == Double.parseDouble(parts[3])) {
                                            //line1.startXProperty().bind(list.get(i).layoutXProperty());
                                            //line1.startYProperty().bind(list.get(i).layoutYProperty());
                                        } else if (list.get(i).layoutXProperty().getValue() == Double.parseDouble(parts[4])) {
                                            line1.endXProperty().bind(list.get(i).layoutXProperty());
                                            line1.endYProperty().bind(list.get(i).layoutYProperty());
                                        } else if (list.get(i).layoutXProperty().getValue() == Double.parseDouble(parts[5])) {
                                            line1.endXProperty().bind(list.get(i).layoutXProperty());
                                            line1.endYProperty().bind(list.get(i).layoutYProperty());
                                        }
                                    }
                                }


                                float angle = (float) Math.toDegrees(Math.atan2(line1.startYProperty().getValue() - line1.endYProperty().getValue(), line1.startXProperty().getValue() - line1.endXProperty().getValue()));

                                angle = angle < 0 ? angle + 180 : angle - 180;

                                polygon.getTransforms().add(new Rotate(angle, 0, 0));
                                polyline.getTransforms().add(new Rotate(angle, 0, 0));
                            }
                        }

                        // Always close files.
                        bufferedReader.close();
                    } catch (FileNotFoundException ex) {
                        System.out.println("Unable to open file '" + selectedFile + "'");
                    } catch (IOException ex) {
                        System.out.println("Error reading file '" + selectedFile + "'");
                        // Or we could just do this:
                        // ex.printStackTrace();
                    }
                } else {

                }
            }
        });


        //Creates new classbox
        itemAddCB.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                classboxFactory factory = new classboxFactory();
                rectangle = factory.createClassbox(root, scene);
            }
        });


        //Sets global linetype value and changes the crosshair to indicate the specific line function being in use (for each line function)
        Association.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    lineType = "Association";
                    scene.setCursor(Cursor.CROSSHAIR);
                    drawingLine = true;
                } catch (Exception e) {

                }
            }
        });


        dAssociation.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    lineType = "Directed Association";
                    scene.setCursor(Cursor.CROSSHAIR);
                    drawingLine = true;
                } catch (Exception e) {

                }
            }
        });

        Inheritance.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    lineType = "Inheritance";
                    scene.setCursor(Cursor.CROSSHAIR);
                    drawingLine = true;
                } catch (Exception e) {

                }
            }
        });

        Dependency.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    lineType = "Dependency";
                    scene.setCursor(Cursor.CROSSHAIR);
                    drawingLine = true;
                } catch (Exception e) {

                }
            }
        });

        Aggregation.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    lineType = "Aggregation";
                    scene.setCursor(Cursor.CROSSHAIR);
                    drawingLine = true;
                } catch (Exception e) {

                }
            }
        });

        Composition.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    lineType = "Composition";
                    scene.setCursor(Cursor.CROSSHAIR);
                    drawingLine = true;
                } catch (Exception e) {

                }
            }
        });


        //Sets cursor to redX to indicate object can be deleted
        itemDeleteObj.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                scene.setCursor(new ImageCursor(image));
            }
        });


        //Clears the pane of all objects
        itemDeleteAll.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clearGUI gui = new clearGUI();
                gui.clearTheGUI(primaryStage);
            }
        });
    }


    private void SaveFile(String content, File file) {
        try {
            FileWriter fileWriter = null;

            fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();
        primaryStage.setTitle("Business Fish UML Diagram Maker");
        primaryStage.getIcons().add(new Image("sample/Business-Fish-hell.jpg"));
    }

    private void ShowPopup(String title, String header, String message){
        Alert helpPopup = new Alert(Alert.AlertType.INFORMATION);
        helpPopup.setTitle(title);
        helpPopup.setHeaderText(header);
        helpPopup.setContentText(message);
        helpPopup.showAndWait();
    }


    public static void main(String[] args) {
        launch(args);
    }
}