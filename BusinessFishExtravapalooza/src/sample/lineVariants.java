package sample;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;

import java.util.Arrays;

/**
 * Created by Brian Raybin on 11/18/2015.
 */
public class lineVariants {

    public Node[] returnLine(Group root)
    {
        final Node[] link = getLine(root);

        return link;
    }


    public Node[] getLine(Group root)
    {
        Polygon polygon = new Polygon();
        Polyline polyline = new Polyline();

        final Line link = new Line();
        link.setStartX(0);
        link.setStartY(0);
        link.setEndX(5.5);
        link.setEndY(-5.5);
        link.setFill(Color.RED);
        link.setStrokeWidth(3);


        link.setOnMouseMoved(evt -> {
            link.setStrokeWidth(10);
        });

        link.setOnMouseExited(evt -> {
            link.setStrokeWidth(3);
        });


        String lineType = Main.lineType;


        //Creates polygon/polyline depending on user arrow choice, and binds it to the line
        if (lineType == "Directed Association")
        {
            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polyline.getPoints().addAll(new Double[]{
                        -25.0, 15.0,
                        0.0, 0.0,
                        -25.0, -15.0});

                polyline.layoutXProperty().bind(link.endXProperty());
                polyline.layoutYProperty().bind(link.endYProperty());
                polyline.setStrokeWidth(3);
                polyline.setStroke(Color.BLACK);
            }
            else
            {
                polyline.getPoints().addAll(new Double[]{
                        25.0, -15.0,
                        0.0, 0.0,
                        25.0, 15.0 });

                polyline.layoutXProperty().bind(link.endXProperty());
                polyline.layoutYProperty().bind(link.endYProperty());
                polyline.setStrokeWidth(3);
                polyline.setStroke(Color.BLACK);
            }
        }
        else if (lineType == "Inheritance")
        {
            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        -25.0, 15.0,
                        -25.0, -15.0 });

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.WHITE);
            }
            else
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        25.0, -15.0,
                        25.0, 15.0 });

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.WHITE);
            }
        }
        else if (lineType == "Dependency")
        {
            link.getStrokeDashArray().addAll(25d, 10d);

            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polyline.getPoints().addAll(new Double[]{
                        -25.0, 15.0,
                        0.0, 0.0,
                        -25.0, -15.0});

                polyline.layoutXProperty().bind(link.endXProperty());
                polyline.layoutYProperty().bind(link.endYProperty());
                polyline.setStrokeWidth(3);
                polyline.setStroke(Color.BLACK);
            }
            else
            {
                polyline.getPoints().addAll(new Double[]{
                        25.0, -15.0,
                        0.0, 0.0,
                        25.0, 15.0});

                polyline.layoutXProperty().bind(link.endXProperty());
                polyline.layoutYProperty().bind(link.endYProperty());
                polyline.setStrokeWidth(3);
                polyline.setStroke(Color.BLACK);
            }
        }
        else if (lineType == "Aggregation")
        {
            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        -25.0, 15.0,
                        -50.0, 0.0,
                        -25.0, -15.0});

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.WHITE);
            }
            else
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        25.0, -15.0,
                        50.0, 0.0,
                        25.0, 15.0 });

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.WHITE);
            }
        }
        else if (lineType == "Composition")
        {
            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        -25.0, 15.0,
                        -50.0, 0.0,
                        -25.0, -15.0});

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.BLACK);
            }
            else
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        25.0, -15.0,
                        50.0, 0.0,
                        25.0, 15.0 });

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.BLACK);
            }
        }

        root.getChildren().addAll(link, polygon, polyline);
        link.toBack();

        Node[] list = new Node[]{link, polygon, polyline};
        return list;
    }


    public Node[] lineHelper(Group root)
    {
        final Line link = new Line();
        link.setStartX(0);
        link.setStartY(0);
        link.setEndX(5.5);
        link.setEndY(-5.5);
        link.setFill(Color.RED);
        link.setStrokeWidth(3);

        String lineType = Main.lineType;


        link.setOnMouseMoved(evt -> {
            link.setStrokeWidth(10);
        });

        link.setOnMouseExited(evt -> {
            link.setStrokeWidth(3);
        });

        Polygon polygon = new Polygon();
        Polyline polyline = new Polyline();

        if (lineType.trim().equals("Directed Association"))
        {
            if (link.startXProperty().get() < link.endXProperty().get()) {
                polyline.getPoints().addAll(new Double[]{
                        -25.0, 15.0,
                        0.0, 0.0,
                        -25.0, -15.0});

                int guess = 0;

                polyline.layoutXProperty().bind(link.endXProperty());
                polyline.layoutYProperty().bind(link.endYProperty());
                polyline.setStrokeWidth(3);
                polyline.setStroke(Color.BLACK);
            }
            else
            {
                polyline.getPoints().addAll(new Double[]{
                        25.0, -15.0,
                        0.0, 0.0,
                        25.0, 15.0 });

                polyline.layoutXProperty().bind(link.endXProperty());
                polyline.layoutYProperty().bind(link.endYProperty());
                polyline.setStrokeWidth(3);
                polyline.setStroke(Color.BLACK);
            }
        }
        else if (lineType.trim().equals("Inheritance"))
        {
            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        -25.0, 15.0,
                        -25.0, -15.0 });

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.WHITE);
            }
            else
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        25.0, -15.0,
                        25.0, 15.0 });

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.WHITE);
            }
        }
        else if (lineType.trim().equals("Dependency"))
        {
            link.getStrokeDashArray().addAll(25d, 10d);

            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polyline.getPoints().addAll(new Double[]{
                        -25.0, 15.0,
                        0.0, 0.0,
                        -25.0, -15.0});

                polyline.layoutXProperty().bind(link.endXProperty());
                polyline.layoutYProperty().bind(link.endYProperty());
                polyline.setStrokeWidth(3);
                polyline.setStroke(Color.BLACK);
            }
            else
            {
                polyline.getPoints().addAll(new Double[]{
                        25.0, -15.0,
                        0.0, 0.0,
                        25.0, 15.0});

                polyline.layoutXProperty().bind(link.endXProperty());
                polyline.layoutYProperty().bind(link.endYProperty());
                polyline.setStrokeWidth(3);
                polyline.setStroke(Color.BLACK);
            }
        }
        else if (lineType.trim().equals("Aggregation"))
        {
            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        -25.0, 15.0,
                        -50.0, 0.0,
                        -25.0, -15.0});

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.WHITE);
            }
            else
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        25.0, -15.0,
                        50.0, 0.0,
                        25.0, 15.0 });

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.WHITE);
            }
        }
        else if (lineType.trim().equals("Composition"))
        {
            if (link.startXProperty().get() < link.endXProperty().get())
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        -25.0, 15.0,
                        -50.0, 0.0,
                        -25.0, -15.0});

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.BLACK);
            }
            else
            {
                polygon.getPoints().addAll(new Double[]{
                        0.0, 0.0,
                        25.0, -15.0,
                        50.0, 0.0,
                        25.0, 15.0 });

                polygon.layoutXProperty().bind(link.endXProperty());
                polygon.layoutYProperty().bind(link.endYProperty());
                polygon.setStrokeWidth(3);
                polygon.setStroke(Color.BLACK);
                polygon.setFill(Color.BLACK);
            }
        }

        Node[] nodes = {link, polygon, polyline};
        root.getChildren().addAll(link, polygon, polyline);

        link.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (root.getScene().getCursor().toString() == "CUSTOM") {

                    root.getChildren().removeAll(nodes);
                    Arrays.fill(Main.globalCircle, null);
                    root.getScene().setCursor(Cursor.DEFAULT);
                }
            }
        });

        return nodes;
    }
}
