package sample;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;

import java.util.Arrays;

/**
 * Created by Brian Raybin on 10/19/2015.
 */
public class drawLine {

    public void Line(Group root, Scene scene) {

        //calls class that creates line/arrowhead variant depending on user choice
        lineVariants variants = new lineVariants();
        javafx.scene.Node[] nodeList = variants.returnLine(root);

        Line line = (Line) nodeList[0];


        //if in delete mode, line and arrowhead will be removed on line click
        line.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (root.getScene().getCursor().toString() == "CUSTOM") {

                    root.getChildren().removeAll(nodeList);
                    Arrays.fill(Main.globalCircle, null);
                    root.getScene().setCursor(Cursor.DEFAULT);
                }
            }
        });


        //when two circles have been clicked (global circle array is full) line will be created and bound to circles' locations
        for (int i = 0; i < Main.globalCircle.length; ++i) {
            if (i == 0)
            {

            }
            else
            {
                line.startXProperty().bind(Main.globalCircle[0].layoutXProperty());
                line.startYProperty().bind(Main.globalCircle[0].layoutYProperty());
                line.endXProperty().bind(Main.globalCircle[1].layoutXProperty());
                line.endYProperty().bind(Main.globalCircle[1].layoutYProperty());

                line.setId(Main.lineType + ", " + line.startXProperty().getValue() + ", " + line.startYProperty().getValue() + ", " + line.endXProperty().getValue() + ", " + line.endYProperty().getValue());
                line.setAccessibleText(Main.lineType);

                float angle = (float) Math.toDegrees(Math.atan2(line.startYProperty().getValue() - line.endYProperty().getValue(), line.startXProperty().getValue() - line.endXProperty().getValue()));

                angle = angle < 0 ? angle + 180 : angle - 180;

                nodeList[1].getTransforms().add(new Rotate(angle, 0, 0));
                nodeList[2].getTransforms().add(new Rotate(angle, 0, 0));


                Arrays.fill(Main.globalCircle, null);
                Main.drawingLine = false;
                scene.setCursor(Cursor.DEFAULT);
            }
        }
    }
}
