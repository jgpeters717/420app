package sample;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Brian Raybin on 10/17/2015.
 */
public class classboxFactory {

    private String coordinates;
    private boolean hasMovedRectangle;
    private Label rectangleTitle = new Label();
    private double initX;
    private double initY;
    private Node[] classbox = new Node[7];


    private Label[] attributes = new Label[10];
    private int attributeNumber = 0;


    private Label[] operations = new Label[10];
    private int operationNumber = 0;

    //function call for rectangle
    public Rectangle createClassbox(Group root, Scene scene) {
        final Rectangle c1 = createRectangle(150, 50, 250, 50, root, scene);
        return c1;
    }


    private Rectangle createRectangle(int length, int width, int x, int y, Group root, Scene scene) {

        for (int i = 0; i < 10; ++i) {
            Label label = new Label();
            attributes[i] = label;

            Label label1 = new Label();
            operations[i] = label1;
        }


        //creates shapes that comprise classbox
        final Rectangle rect1 = new Rectangle(length, width);
        final Rectangle rect2 = new Rectangle(length, width);
        final Rectangle rect3 = new Rectangle(length, width);
        Circle circleLeft = new Circle(10, Color.rgb(0, 255, 0, 0.5));
        Circle circleRight = new Circle(10, Color.rgb(0, 255, 0, 0.5));
        Circle circleBottomLeft = new Circle(10, Color.rgb(0, 255, 0, 0.5));
        Circle circleBottomRight = new Circle(10, Color.rgb(0, 255, 0, 0.5));

        //helper function for recreating a scene from file
        helperOpen(rect1, rect2, rect3, circleLeft, circleRight, circleBottomLeft, circleBottomRight);


        //shapes layout
        rect1.setLayoutX(x);
        rect1.setLayoutY(y);
        rect2.setLayoutX(x);
        rect2.setLayoutY(y * 2);
        rect3.setLayoutX(x);
        rect3.setLayoutY(y * 3);
        circleLeft.setLayoutX(x);
        circleLeft.setLayoutY(y);
        circleRight.setLayoutX(x + rect1.getWidth());
        circleRight.setLayoutY(y);
        circleBottomLeft.setLayoutX(x);
        circleBottomLeft.setLayoutY(y * 4);
        circleBottomRight.setLayoutX(x + rect1.getWidth());
        circleBottomRight.setLayoutY(y * 4);


        Double rectX = rect1.getLayoutX();
        Double rectY = rect1.getLayoutY();

        String rectangleTitleInfo = rectangleTitle.getText();
        rect1.setAccessibleText(rectangleTitleInfo + ", ");

        coordinates = rectX.toString() + ", " + rectY.toString();

        rect1.setId(coordinates);


        rect1.setFill(Color.TRANSPARENT);
        rect1.setStroke(Color.BLACK);
        rect1.setStrokeWidth(2);

        rect2.setFill(Color.TRANSPARENT);
        rect2.setStroke(Color.BLACK);
        rect2.setStrokeWidth(2);

        rect3.setFill(Color.TRANSPARENT);
        rect3.setStroke(Color.BLACK);
        rect3.setStrokeWidth(2);


        rectangleTitle.setFont(Font.font("Verdana", 20));


        handleRectangle1(rect1, rect2, rect3, circleLeft, circleRight, circleBottomLeft, circleBottomRight, root);
        handleRectangle2(rect2, root, rect1);
        handleRectangle3(rect3, root, rect1);
        handleCircle(circleLeft, circleRight, circleBottomLeft, circleBottomRight, root, scene);


        root.getChildren().addAll(rect1, rect2, rect3, circleLeft, circleRight, circleBottomLeft, circleBottomRight);

        return rect1;
    }


    public void handleRectangle1(Rectangle rect1, Rectangle rect2, Rectangle rect3, Circle circleLeft, Circle circleRight, Circle circleBottomLeft, Circle circleBottomRight, Group root) {

        //Red X displayed on hover if delete function was called, background changed to green to indicate shape has been entered
        rect1.setOnMouseMoved(evt -> {
            rect1.setFill(Color.rgb(0, 255, 0, 0.3));
            try {
                if (root.getScene().getCursor().toString() == "CUSTOM") {
                    Image image = new Image("sample/redX.png");  //pass in the image path
                    rect1.setCursor(new ImageCursor(image));

                    rect1.setFill(Color.TRANSPARENT);
                }
            } catch (NullPointerException e) {
                rect1.setCursor(Cursor.HAND);
            }
        });


        rect1.setOnMouseExited(evt -> {
            rect1.setFill(Color.TRANSPARENT);
        });


        //sets dialog box options when rectangle is clicked
        rect1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                try {
                    if (root.getScene().getCursor().toString() == "CUSTOM") {
                        root.getChildren().removeAll(rect1, rect2, rect3, circleLeft, circleRight, rectangleTitle);

                        for (int i = 0; i < attributes.length; ++i) {
                            root.getChildren().remove(attributes[i]);
                            root.getChildren().remove(operations[i]);
                        }

                        Arrays.fill(attributes, null);
                        Arrays.fill(operations, null);

                        root.getScene().setCursor(Cursor.DEFAULT);
                    } else {
                        if (hasMovedRectangle == false) {
                            TextInputDialog dialog = new TextInputDialog("Title");
                            dialog.setTitle("Title Dialog");
                            dialog.setHeaderText("Input Your Title Name");
                            dialog.setContentText("New Title:");

                            Optional<String> result = dialog.showAndWait();

                            if (result.isPresent()) {
                                //TITLE NEEDS TO BE CENTERED, ISSUE IF IT IS LONGER
                                rectangleTitle.setText(result.get());
                                rectangleTitle.setLayoutX(rect1.getLayoutX() + rect1.getWidth() / 3);
                                rectangleTitle.setLayoutY(rect1.getLayoutY() + rect1.getHeight() / 4);

                                String rectangleTitleInfo = rectangleTitle.getText();
                                rect1.setAccessibleText(rectangleTitleInfo + ", ");

                                root.getChildren().add(rectangleTitle);
                            }
                        } else {
                            hasMovedRectangle = false;
                        }
                    }
                } catch (NullPointerException e) {
                    if (hasMovedRectangle == false) {
                        TextInputDialog dialog = new TextInputDialog("Title");
                        dialog.setTitle("Title Dialog");
                        dialog.setHeaderText("Input Your Title Name");
                        dialog.setContentText("New Title:");

                        Optional<String> result = dialog.showAndWait();

                        if (result.isPresent()) {
                            //TITLE NEEDS TO BE CENTERED, ISSUE IF IT IS LONGER
                            rectangleTitle.setText(result.get());
                            rectangleTitle.setLayoutX(rect1.getLayoutX() + rect1.getWidth() / 3);
                            rectangleTitle.setLayoutY(rect1.getLayoutY() + rect1.getHeight() / 4);

                            String rectangleTitleInfo = rectangleTitle.getText();
                            rect1.setAccessibleText(rectangleTitleInfo + ", ");

                            root.getChildren().add(rectangleTitle);
                        }
                    } else {
                        hasMovedRectangle = false;
                    }
                }
            }
        });


        //sets shape coordinate layouts depending on where rectangle one is dragged
        rect1.setOnMouseDragged(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                hasMovedRectangle = true;

                double dragX = me.getSceneX();
                double dragY = me.getSceneY();


                double newXPosition = initX + dragX;
                double newYPosition = initY + dragY;


                Double rectX = rect1.getLayoutX();
                Double rectY = rect1.getLayoutY();

                String rectangleTitleInfo = rectangleTitle.getText();
                rect1.setAccessibleText(rectangleTitleInfo + ", ");

                coordinates = rectX.toString() + ", " + rectY.toString();

                rect1.setId(coordinates);


                rect1.setLayoutX(newXPosition);
                rect2.setLayoutX(newXPosition);
                rect3.setLayoutX(newXPosition);
                circleLeft.setLayoutX(newXPosition);
                circleRight.setLayoutX(newXPosition + rect1.getWidth());
                circleBottomLeft.setLayoutX(newXPosition);
                circleBottomRight.setLayoutX(newXPosition + rect1.getWidth());



                rect1.setLayoutY(newYPosition);
                rect2.setLayoutY(newYPosition + rect1.getHeight());
                rect3.setLayoutY(newYPosition + (rect1.getHeight()) * 2);
                circleLeft.setLayoutY(newYPosition);
                circleRight.setLayoutY(newYPosition);
                circleBottomLeft.setLayoutY(newYPosition + rect1.getHeight() * 3);
                circleBottomRight.setLayoutY(newYPosition + rect1.getHeight() * 3);


                rectangleTitle.setLayoutX(newXPosition + rect1.getWidth() / 3);
                rectangleTitle.setLayoutY(newYPosition + rect1.getHeight() / 4);


                try {
                    for (int i = 0; i < 10; ++i) {
                        attributes[i].setLayoutX(newXPosition + 5);
                        attributes[i].setLayoutY(newYPosition + rect2.getHeight() + i * 12);
                    }
                } catch (Exception e) {

                }


                try {
                    for (int i = 0; i < 10; ++i) {
                        operations[i].setLayoutX(newXPosition + 5);
                        operations[i].setLayoutY(newYPosition + rect2.getHeight() + rect3.getHeight() + i * 12);
                    }
                } catch (Exception e) {

                }
            }
        });


        rect1.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                //change the z-coordinate of the circle
                rect1.toFront();
            }
        });


        //resets line/label coordinates within accessible text value that will be put in text file on save
        rect1.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                Double rectX = rect1.getLayoutX();
                Double rectY = rect1.getLayoutY();

                String rectangleTitleInfo = rectangleTitle.getText();
                rect1.setAccessibleText(rectangleTitleInfo + ", ");

                coordinates = rectX.toString() + ", " + rectY.toString();

                rect1.setId(coordinates);

                ObservableList<Node> listy = root.getChildren();

                for (int i = 0; i < listy.size(); ++i) {
                    if (listy.get(i) instanceof Line) {
                        Line line = (Line) listy.get(i);
                        String typer = line.getAccessibleText();

                        if (line.getEndX() == rect1.layoutXProperty().getValue()) {
                            line.setId(typer + ", " + line.getStartX() + ", " + line.getStartY() + ", " + line.getEndX() + ", " + line.getEndY());
                        } else if (line.getEndY() == rect1.layoutYProperty().getValue()) {
                            line.setId(typer + ", " + line.getStartX() + ", " + line.getStartY() + ", " + line.getEndX() + ", " + line.getEndY());
                        }
                    } else if (listy.get(i) instanceof Label) {
                        Label label = (Label) listy.get(i);
                        String labelType = label.getAccessibleText();
                        label.setId(labelType + ", " + label.getLayoutX() + ", " + label.getLayoutY());
                    }
                }


                try {
                    int polyShapeNode = 0;
                    String shapeType = "";

                    Double polylineXCoordinate = 0.0;
                    Double polylineYCoordinate = 0.0;


                    ObservableList<Node> list = root.getChildren();

                    for (int i = 0; i < list.size(); ++i) {
                        if (list.get(i) instanceof Polyline) {
                            polylineXCoordinate = list.get(i).getLayoutX();
                            polylineYCoordinate = list.get(i).getLayoutY();

                            polyShapeNode = i;
                            shapeType = "Polyline";

                            arrowheadRotation(list, polylineXCoordinate, polylineYCoordinate, polyShapeNode, shapeType);
                        } else if (list.get(i) instanceof Polygon) {
                            polylineXCoordinate = list.get(i).getLayoutX();
                            polylineYCoordinate = list.get(i).getLayoutY();

                            polyShapeNode = i;
                            shapeType = "Polygon";

                            arrowheadRotation(list, polylineXCoordinate, polylineYCoordinate, polyShapeNode, shapeType);
                        }
                    }
                } catch (Exception e) {

                }
            }
        });


        rect1.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                //when mouse is pressed, store initial position
                initX = rect1.getTranslateX();
                initY = rect1.getTranslateY();
            }
        });
    }


    //calculates rotation for polygons/polylines
    public void arrowheadRotation(ObservableList<Node> list, Double polylineXCoordinate, Double polylineYCoordinate, int polyShapeNode, String shapeType) {

        int lineNode = 0;

        Double matchedLineStartXCoordinate = 0.0;
        Double matchedLineStartYCoordinate = 0.0;
        Double matchedLineXCoordinate = 0.0;
        Double matchedLineYCoordinate = 0.0;

        for (int j = 0; j < list.size(); ++j) {
            if (list.get(j) instanceof Line) {
                matchedLineXCoordinate = ((Line) list.get(j)).endXProperty().getValue();
                matchedLineYCoordinate = ((Line) list.get(j)).endYProperty().getValue();

                Boolean matchesX = (polylineXCoordinate.equals(matchedLineXCoordinate)) ? true : false;
                Boolean matchesY = (polylineYCoordinate.equals(matchedLineYCoordinate)) ? true : false;

                lineNode = j;

                if ((matchesX == true) && (matchesY == true)) {
                    matchedLineStartXCoordinate = ((Line) list.get(lineNode)).startXProperty().getValue();
                    matchedLineStartYCoordinate = ((Line) list.get(lineNode)).startYProperty().getValue();

                    float angle = (float) Math.toDegrees(Math.atan2(matchedLineStartYCoordinate - matchedLineYCoordinate, matchedLineStartXCoordinate - matchedLineXCoordinate));

                    ObservableList<Transform> list1 = null;

                    if (shapeType == "Polyline") {
                        list1 = ((Polyline) list.get(polyShapeNode)).getTransforms();
                    } else if (shapeType == "Polygon") {
                        list1 = ((Polygon) list.get(polyShapeNode)).getTransforms();
                    }

                    double currentAngle = 0;

                    int listSize = list1.size();

                    if (listSize >= 1) {

                        currentAngle = ((Rotate) list1.get(listSize - 1)).getAngle();

                        list.get(polyShapeNode).getTransforms().add(new Rotate(-currentAngle));

                        //System.out.println("previous angle is " + currentAngle);

                        angle = angle < 0 ? angle + 180 : angle - 180;

                        list.get(polyShapeNode).getTransforms().add(new Rotate(angle, 0, 0));
                    } else {

                        //System.out.println("current angle is " + angle);

                        angle = angle < 0 ? angle + 180 : angle - 180;

                        list.get(polyShapeNode).getTransforms().add(new Rotate(angle, 0, 0));
                    }
                }
            }
        }
    }


    //handles rectangle2 color/dialog options/stores attribute variables for save function
    public void handleRectangle2(Rectangle rect2, Group root, Rectangle rect1) {
        rect2.setCursor(Cursor.HAND);

        rect2.setOnMouseMoved(evt -> {
            rect2.setFill(Color.rgb(0, 255, 0, 0.3));
        });

        rect2.setOnMouseExited(evt -> {
            rect2.setFill(Color.TRANSPARENT);
        });

        rect2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                TextInputDialog dialog = new TextInputDialog("Attributes");
                dialog.setTitle("Attribute Dialog");
                dialog.setHeaderText("Input Attributes");
                dialog.setContentText("New Attribute:");

                Optional<String> result = dialog.showAndWait();

                if (result.isPresent()) {
                    attributes[attributeNumber].setText(result.get());


                    String rectAttribute = rect1.getAccessibleRoleDescription();
                    if (rectAttribute != null)
                    {
                        rectAttribute += "ATTR" + result.get() + ", ";
                        rect1.setAccessibleRoleDescription(rectAttribute);
                    }
                    else
                    {
                        rectAttribute = "ATTR" + result.get() + ", ";
                        rect1.setAccessibleRoleDescription(rectAttribute);
                    }



                    attributes[attributeNumber].setLayoutX(rect2.getLayoutX() + 5);
                    attributes[attributeNumber].setLayoutY(rect2.getLayoutY() + attributeNumber * 12);
                    root.getChildren().add(attributes[attributeNumber]);
                    attributeNumber++;
                }
            }
        });
    }


    //handles rectangle3 color/dialog options/stores operation variables for save function
    public void handleRectangle3(Rectangle rect3, Group root, Rectangle rect1) {
        rect3.setCursor(Cursor.HAND);

        rect3.setOnMouseMoved(evt -> {
            rect3.setFill(Color.rgb(0, 255, 0, 0.3));
        });

        rect3.setOnMouseExited(evt -> {
            rect3.setFill(Color.TRANSPARENT);
        });

        rect3.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                TextInputDialog dialog = new TextInputDialog("Operations");
                dialog.setTitle("Operation Dialog");
                dialog.setHeaderText("Input Operations");
                dialog.setContentText("New Operation:");

                Optional<String> result = dialog.showAndWait();

                if (result.isPresent()) {
                    operations[operationNumber].setText(result.get());

                    String rectOperation = rect1.getAccessibleRoleDescription();
                    if (rectOperation != null)
                    {
                        rectOperation += "OPER" + result.get() + ", ";
                        rect1.setAccessibleRoleDescription(rectOperation);
                    }
                    else
                    {
                        rectOperation = "OPER" + result.get() + ", ";
                        rect1.setAccessibleRoleDescription(rectOperation);
                    }

                    operations[operationNumber].setLayoutX(rect3.getLayoutX() + 5);
                    operations[operationNumber].setLayoutY(rect3.getLayoutY() + operationNumber * 12);
                    root.getChildren().add(operations[operationNumber]);
                    operationNumber++;
                }
            }
        });
    }


    //helper function stores shapes within a list that can be accessed during recreation from text file
    public void helperOpen(Rectangle rect1, Rectangle rect2, Rectangle rect3, Circle circleLeft, Circle circleRight, Circle circleBottomLeft, Circle circleBottomRight) {
        classbox[0] = rect1;
        classbox[1] = rect2;
        classbox[2] = rect3;
        classbox[3] = circleLeft;
        classbox[4] = circleRight;
        classbox[5] = circleBottomLeft;
        classbox[6] = circleBottomRight;
    }


    //fucntion resets based on coordinates supplied from text file
    public void onOpen(Double fillerX, Double fillerY, String rectTitle, Group root, Label[] attributes, Label[] operations) {
        Rectangle rect1 = (Rectangle) classbox[0];
        Rectangle rect2 = (Rectangle) classbox[1];
        Rectangle rect3 = (Rectangle) classbox[2];
        Circle circleLeft = (Circle) classbox[3];
        Circle circleRight = (Circle) classbox[4];
        Circle circleBottomLeft = (Circle) classbox[5];
        Circle circleBottomRight = (Circle) classbox[6];

        rect1.setLayoutX(fillerX);
        rect2.setLayoutX(fillerX);
        rect3.setLayoutX(fillerX);
        circleLeft.setLayoutX(fillerX);
        circleRight.setLayoutX(fillerX + rect1.getWidth());
        circleBottomLeft.setLayoutX(fillerX);
        circleBottomRight.setLayoutX(fillerX + rect1.getWidth());

        Double rectX = rect1.getLayoutX();
        Double rectY = rect1.getLayoutY();

        if (!rectTitle.equals("null"))
        {
            rectangleTitle.setText(rectTitle);
        }


        String rectangleTitleInfo = rectangleTitle.getText();
        rect1.setAccessibleText(rectangleTitleInfo + ", ");

        coordinates = rectX.toString() + ", " + rectY.toString();

        rect1.setId(coordinates);


        rect1.setLayoutY(fillerY);
        rect2.setLayoutY(fillerY + rect1.getHeight());
        rect3.setLayoutY(fillerY + (rect1.getHeight()) * 2);
        circleLeft.setLayoutY(fillerY);
        circleRight.setLayoutY(fillerY);
        circleBottomLeft.setLayoutY(fillerY + rect1.getHeight()*3);
        circleBottomRight.setLayoutY(fillerY + rect1.getHeight()*3);



        rectangleTitle.setLayoutX(fillerX + rect1.getWidth() / 3);
        rectangleTitle.setLayoutY(fillerY + rect1.getHeight() / 4);

        root.getChildren().add(rectangleTitle);

        try {
            for (int i = 0; i < 10; ++i) {
                attributes[i].setLayoutX(fillerX + 5);
                attributes[i].setLayoutY(fillerY + rect2.getHeight() + i * 12);
            }
        } catch (Exception e) {

        }


        try {
            for (int i = 0; i < 10; ++i) {
                operations[i].setLayoutX(fillerX + 5);
                operations[i].setLayoutY(fillerY + rect2.getHeight() + rect3.getHeight() + i * 12);
            }
        } catch (Exception e) {

        }

        for (int i = 0; i < attributes.length; ++i) {
            root.getChildren().addAll(attributes[i], operations[i]);
        }
    }


    //handles circles border changes/locations/onclick events
    public void handleCircle(Circle circleLeft, Circle circleRight, Circle circleBottomLeft, Circle circleBottomRight, Group root, Scene scene) {
        //circleLeft.setCursor(Cursor.HAND);
        //circleRight.setCursor(Cursor.HAND);

        circleLeft.setOnMouseMoved(evt -> {
            circleLeft.setStroke(Color.GREEN);
            circleLeft.setRadius(15);
        });

        circleLeft.setOnMouseExited(evt -> {
            circleLeft.setStroke(Color.TRANSPARENT);
            circleLeft.setRadius(10);
        });

        circleRight.setOnMouseMoved(evt -> {
            circleRight.setStroke(Color.GREEN);
            circleRight.setRadius(15);
        });

        circleRight.setOnMouseExited(evt -> {
            circleRight.setStroke(Color.TRANSPARENT);
            circleRight.setRadius(10);
        });

        circleBottomLeft.setOnMouseMoved(event -> {
            circleBottomLeft.setStroke(Color.GREEN);
            circleBottomLeft.setRadius(15);
        });

        circleBottomLeft.setOnMouseExited(event -> {
            circleBottomLeft.setStroke(Color.GREEN);
            circleBottomLeft.setRadius(10);
        });

        circleBottomRight.setOnMouseMoved(event -> {
            circleBottomRight.setStroke(Color.GREEN);
            circleBottomRight.setRadius(15);
        });

        circleBottomRight.setOnMouseExited(event -> {
            circleBottomRight.setStroke(Color.GREEN);
            circleBottomRight.setRadius(10);
        });


        circleLeft.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (Main.drawingLine = true) {
                    for (int i = 0; i < Main.globalCircle.length; ++i) {
                        if (Main.globalCircle[i] == null) {
                            Main.globalCircle[i] = circleLeft;

                            if (i == Main.globalCircle.length - 1) {
                                drawLine line = new drawLine();
                                line.Line(root, scene);
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        });


        circleRight.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (Main.drawingLine = true) {
                    for (int i = 0; i < Main.globalCircle.length; ++i) {
                        if (Main.globalCircle[i] == null) {
                            Main.globalCircle[i] = circleRight;

                            if (i == Main.globalCircle.length - 1) {
                                drawLine line = new drawLine();
                                line.Line(root, scene);
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        });


        circleBottomLeft.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (Main.drawingLine = true) {
                    for (int i = 0; i < Main.globalCircle.length; ++i) {
                        if (Main.globalCircle[i] == null) {
                            Main.globalCircle[i] = circleBottomLeft;

                            if (i == Main.globalCircle.length - 1) {
                                drawLine line = new drawLine();
                                line.Line(root, scene);
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        });


        circleBottomRight.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (Main.drawingLine = true) {
                    for (int i = 0; i < Main.globalCircle.length; ++i) {
                        if (Main.globalCircle[i] == null) {
                            Main.globalCircle[i] = circleBottomRight;

                            if (i == Main.globalCircle.length - 1) {
                                drawLine line = new drawLine();
                                line.Line(root, scene);
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        });
    }
}
